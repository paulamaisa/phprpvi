<?php

// Cristiane
defined('BASEPATH') OR exit('No direct script access allowed');

class Cardapios extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('cardapios_model');
    }

    public function index() {
        $this->listar();
    }

    public function listar() {
        $this->load->model('cardapios_model');

        $data['registros'] = $this->cardapios_model->getAll();
        $this->load->view('listar_view', $data);
    }

    public function novo() {
        $this->load->view('cadastro_view');
    }

    public function salvar() {
        $dados = array();
        $dados['codigo'] = $_POST['txtCodigo'];
        $dados['datarefeicao'] = $_POST['txtData'];
        $dados['tipoDaRefeicao'] = $_POST['txtRefeicao'];
        $dados['pratoprincipal'] = $_POST['txtPratoP'];
        $dados['acompanhamento'] = $_POST['txtAcomp'];
        $dados['opcaovegetariana'] = $_POST['txtOpVegetariana'];
        $dados['guarnicao'] = $_POST['txtGuarnicao'];
        $dados['salada'] = $_POST['txtSalada'];
        $dados['sobremesa'] = $_POST['txtSobremesa'];
        $dados['bebida'] = $_POST['txtBebida'];
        
        if ($dados['datarefeicao'] == '') {
            $data['mensagem'] = "O campo data é obrigatório.";
        } else {
            $ok = false;
            if ($dados['codigo'] != '') {
                $ok = $this->cardapios_model->update($dados);
            } else {
                $ok = $this->cardapios_model->insert($dados);
            }
            if ($ok) {
                $data['mensagem'] = "Registro salvo com sucesso";
            } else {
                $data['mensagem'] = "Não foi possível salvar os dados";
            }
        }
        $data['voltar'] = base_url('index.php/cardapios/listar');
        $this->load->view('mensagem_view', $data);
    }

}
