<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<!--
// Paula Maisa
-->

<html>
    <head>
        <meta charset="UTF-8">
        <title>RUniversitário</title>
    </head>
    <body>
        <?php echo form_open(base_url('index.php/setores/salvar')) ?>
        <h1>Cadastro de refeição</h1>
        <br/>

        <label for="datarefeicao">Data:</label>
        <input type="date" name="txtData" id="datarefeicao"
               value="<?php echo @$Cardapio->datarefeicao ?>" >

        <label for="refeicao">Tipo de refeição:</label>
        <select id="refeicao" name="txtRefeicao">

            <option 
            <?php
            if (@$Cardapio->tipoDaRefeicao == 'A') {
                echo ' selected="selected" ';
            } else {
                echo ' ';
            }
            ?>
                value = "A">Almoço</option>

            <option 
            <?php
            if (@$Cardapio->tipoDaRefeicao == 'J') {
                echo ' selected="selected" ';
            } else {
                echo ' ';
            }
            ?>
                value = "J">Jantar</option>
        </select> 

        <label for = "pratoprincipal" >Prato principal:</label>
        <input type = "text" size = "45" name = "txtPratoP" placeholder = "Ex.: Toscana" id = "pratoprincipal"
               value = "<?php echo @$Cardapio->pratoprincipal ?>">

        <label for = "acompanhamento">Acompanhamento:</label>
        <input type = "text" size = "45" name = "txtAcomp"
               placeholder = "Ex.: Arroz parboilizado, arroz integral e feijão preto"
               id = "acompanhamento"
               value = "<?php echo @$Cardapio->acompanhamento ?>">

        <label for = "opcaovegetariana">Opção vegetariana:</label>
        <input type = "text" name = "txtOpVegetariana" placeholder = "Ex.: Proteína de soja com legumes"
               id = "opcaovegetariana"
               value = "<?php echo @$Cardapio->opcaovegetariana ?>">

        <label for = "guarnicao">Guarnição:</label>
        <input type = "text" name = "txtGuarnicao" placeholder = "Ex.: Coleslaw" id = "guarnicao"
               value = "<?php echo @$Cardapio->guarnicao ?>">

        <label for = "salada">Salada:</label>
        <input type = "text" name = "txtSalada" placeholder = "Ex.: Alface e beterraba" id = "salada"
               value = "<?php echo @$Cardapio->salada ?>">

        <label for = "sobremesa">Sobremesa:</label>
        <input type = "text" name = "txtSobremesa" placeholder = "Ex.: Gelatina" id = "sobremesa"
               value = "<?php echo @$Cardapio->sobremesa ?>">

        <label for = "bebida">Bebida:</label>
        <input type = "text" name = "txtBebida" placeholder = "Ex.: Suco de abacaxi" id = "bebida"
               value = "<?php echo @$Cardapio->bebida ?>">

        <button type = "submit" >Salvar</button>
        <input type = "reset" value = "Limpar" />
        <?php echo form_close() ?>

    </body>
</html>
