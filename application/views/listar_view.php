<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<!--
// Cristiane
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <!--
            Tabela criada pra visualizar o que foi salvo no banco
        -->
        <a href='<?php echo base_url() ?>index.php/cardapios/novo'>Incluir cardápio</a>
        <table border="1">
            <thead>
                <tr>
                    <td>Data</td>
                    <td>Tipo de refeição</td>
                    <td>Prato principal</td>
                    <td>Acompanhamento</td>
                    <td>Opção vegetariana</td>
                    <td>Guarnição</td>
                    <td>Salada</td>
                    <td>Sobremesa</td>
                    <td>Bebida</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($registros as $registro): ?>
                    <tr>
                        <td><?php echo $registro->datarefeicao ?></td>
                        <td><?php
                            if ($registro->tipoDaRefeicao == "A") {
                                echo 'Almoço';
                            }
                            if ($registro->tipoDaRefeicao == "J") {
                                echo 'Jantar';
                            }
                            ?></td>
                        <td><?php echo $registro->pratoprincipal ?></td>
                        <td><?php echo $registro->acompanhamento ?></td>
                        <td><?php echo $registro->opcaovegetariana ?></td>
                        <td><?php echo $registro->guarnicao ?></td>
                        <td><?php echo $registro->salada ?></td>
                        <td><?php echo $registro->sobremesa ?></td>
                        <td><?php echo $registro->bebida ?></td>

                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </body>
</html>
