<?php
//Daniele
class Cardapios_model extends CI_Model {

        public $table='cardapio'; //especificar o nome da tabela no banco
        
        public function __construct() {
                // Call the CI_Model constructor
                parent::__construct();
                
        }
        
        public function insert($dados) {
            return $this->db->insert($this->table, $dados);
        }
        
               
        public function getById($id) {
            $this->db->where('codigo', $id);
            $query = $this->db->get($this->table);
            return $query->first_row();
        }
        
        public function getAll() {
            $query = $this->db->get($this->table);
            return $query->result();
        }
}
?>